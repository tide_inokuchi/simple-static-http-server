import nodeStatic from 'node-static';
import colors from 'colors';
import localIP from 'my-local-ip';
import accessLog from 'access-log';
import portastic from 'portastic';
import emoji from 'node-emoji';

const port = 9999;
const file = new nodeStatic.Server('./public');

// Check specified port is open or not.
portastic.test(port)
  .then((isOpen) => {
    if (!isOpen) {
      console.log(`${emoji.get("poop")}  Port:${port} is closed. Probably already in use.`.red);
      console.log(`Kill other process on port:${port} and hit "rs" here ${emoji.get("+1")}`.red);
      process.exit(0);
    }
    startServer(port);
  })

// Start server
const startServer = (port) => {
  const logFormat = '[:{clfDate}] :method - :url :statusCode :{delta}ms';
  require('http').createServer((req, res) => {
    req.addListener('end', () => {
      file.serve(req, res);
      accessLog(req, res, logFormat, s => console.log(s));
    }).resume();
  }).listen(port);
  console.log(`\n${emoji.get("rocket")}  Server launched at http://${localIP()}:${port}\n`.cyan);
}
