# Dependencies
### Node & npm
|  | ver. |
|:---:|:------|
| node | >=8.5.0 |
| npm | >=5.4.2 |

### JavaScript
You can use ECMA2017.
`babel-cli` & `babel-preset-env` is included :+1:

### CSS
Using `node-sass`, so you can use sass.
If you want to use scss, or don't like `node-sass`, see `package.json` and modify `watch:sass` in scripts.

# Initial setup

```
$ npm install && npm run build
```

# Run server

```
$ npm start
```
